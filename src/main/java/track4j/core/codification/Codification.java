/**
 *
 */
package track4j.core.codification;

/**
 * The @link{Codification} class.
 */
public enum Codification {
    /**
     * Type of codification.
     */
    DERIVATIVE, VECTORIAL_SPACE
}
