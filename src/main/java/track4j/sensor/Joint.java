/**
 *
 */
package track4j.sensor;

/**
 * The @link{Joint} class.
 */
public enum Joint {
    /**
     * Main joints.
     */
    RIGHT_HAND, LEFT_HAND, RIGHT_FOOT, LEFT_FOOT;
}
